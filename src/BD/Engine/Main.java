package BD.Engine;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JOptionPane;

import BD.Frame.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Frame fr = new Frame("BD");
		Learner[] list =new Learner[3];
		list[0] = new Learner("Kim","Chen In","Korean",123,false);
		list[1] = new Learner("Pavel","RRR","Russian",10,true);
		list[2] = new Learner("Vladimir","Flame of Revolution","Socialogy",43,false);
		int[] ages= new int[list.length];
		
		for (int i=0;i<=list.length-1;i++) {
			ages[i]=list[i].getAge();
			
		}
		
		Arrays.sort(ages);
		for (int i : ages) {
			System.out.println(getLearner(i,list));
		}
		//System.out.println(getLearner(ages[2],list).toString());
		
	}
	public static Learner getLearner(int age,Learner[] list) {
		int ind = 0;
		for(int c=0;c<=list.length-1;c++) {
			if(list[c].getAge()==age) {
				ind=c;
				break;
				
				
				
			}
		}
		return list[ind];
		
	}

}
